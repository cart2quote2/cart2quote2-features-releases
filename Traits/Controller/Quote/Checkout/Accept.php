<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Controller\Quote\Checkout;
trait Accept
{
    /**
     * Redirect to customer checkout page if the quotation customer is the same customer as logged in
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    private function execute()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$isDeleted = $this->initQuote()->quote->isCanceled();
        if ($isDeleted) {
            $quoteAcceptMessage = __('This quote is no longer available');
            $this->messageManager->addErrorMessage($quoteAcceptMessage);
            $url = $this->_url->getUrl('quotation/quote/view', ['quote_id' => $this->quote->getId()]);
            return $this->resultRedirectFactory->create()->setUrl($url);
        }
        $this->initQuote();
        if ($this->isSameCustomer()) {
            return $this->proceedToCheckout();
        }
        return $this->defaultRedirect();
		}
	}
}
