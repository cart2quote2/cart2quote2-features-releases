<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Controller\Adminhtml\Quote;
trait Pdf
{
    /**
     * @var \Cart2Quote\Quotation\Model\Quote\Pdf\Quote
     */
    /**
     * @var \Cart2Quote\Quotation\Helper\Pdf\Download
     */
    /**
     * Pdf constructor
     *
     * @param \Cart2Quote\Quotation\Model\ResourceModel\Quote\Collection $quoteCollection
     * @param \Cart2Quote\Quotation\Model\Quote\Pdf\Quote $pdfModel
     * @param \Cart2Quote\Quotation\Helper\Pdf\Download $downloadHelper
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
     * @param \Magento\Store\Model\Store $store
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Framework\Translate\InlineInterface $translateInline
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param \Cart2Quote\Quotation\Helper\Data $helperData
     * @param \Cart2Quote\Quotation\Model\QuoteFactory $quoteFactory
     * @param \Cart2Quote\Quotation\Model\ResourceModel\Status\Collection $statusCollection
     * @param \Cart2Quote\Quotation\Model\Admin\Quote\Create $quoteCreate
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Cart2Quote\Quotation\Helper\Cloning $cloningHelper
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Backend\Model\Session\Quote $backendQuoteSession
     * @param \Magento\Catalog\Helper\Product $productHelper
     * @param \Magento\GiftMessage\Model\Save $giftMessageSave
     * @param \Magento\Framework\Json\Helper\Data $jsonDataHelper
     */
    /**
     * Download PDF for the quotation quote item
     */
    private function execute()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			if ($results = parent::execute()) {
            return $results;
        }
        // ini_set('zlib.output_compression', '0');
        $quote = $this->_initQuote();
        $filePath = $this->pdfModel->createQuotePdf([$quote]);
        $this->downloadHelper->setResource($filePath, \Magento\Downloadable\Helper\Download::LINK_TYPE_FILE);
        $fileName = $this->downloadHelper->getFilename();
        $contentType = $this->downloadHelper->getContentType();
        //$contentDisposition = $this->_downloadHelper->getContentDisposition()
        $contentDisposition = 'attachment';
        $this->getResponse()->setHttpResponseCode(
            200
        )->setHeader(
            'target',
            '_blank',
            true
        )->setHeader(
            'Pragma',
            'public',
            true
        )->setHeader(
            'Cache-Control',
            'private, max-age=0, must-revalidate, post-check=0, pre-check=0',
            true
        )->setHeader(
            'Content-type',
            $contentType,
            true
        );
        if ($fileSize = $this->downloadHelper->getFileSize()) {
            $this->getResponse()->setHeader('Content-Length', $fileSize);
        }
        $this->getResponse()->setHeader('Content-Disposition', $contentDisposition . '; filename=' . $fileName);
        $this->getResponse()->clearBody();
        $this->getResponse()->sendHeaders();
        $this->downloadHelper->output();
		}
	}
    /**
     * ACL check
     *
     * @return bool
     */
    private function _isAllowed()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return $this->_authorization->isAllowed('Cart2Quote_Quotation::actions_view');
		}
	}
}
