<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\Quote;
use Magento\Framework\Exception\NoSuchEntityException;
trait MoveToCart
{
    /**
     * @var \Cart2Quote\Quotation\Helper\StockCheck
     */
    /**
     * @var \Cart2Quote\Quotation\Model\Quote
     */
    /**
     * @var \Magento\Checkout\Model\Session
     */
    /**
     * @var \Cart2Quote\Quotation\Model\QuotationCart
     */
    /**
     * @var \Cart2Quote\Quotation\Model\Session
     */
    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    /**
     * @var \Magento\Framework\UrlInterface
     */
    /**
     * MoveToCart constructor.
     *
     * @param \Cart2Quote\Quotation\Helper\StockCheck $stockCheckHelper
     * @param \Cart2Quote\Quotation\Model\Quote $quotationQuote
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Cart2Quote\Quotation\Model\QuotationCart $quotationCart
     * @param \Cart2Quote\Quotation\Model\Session $quotationSession
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Magento\Framework\UrlInterface $url
     */
    /**
     * Get url
     *
     * @param string $data
     * @return string
     */
    private function getUrl($data = '')
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return $this->url->getUrl($data);
		}
	}
    /**
     * Clone quote model
     *
     * @return bool|\Magento\Quote\Model\Quote
     * @throws NoSuchEntityException
     */
    private function cloneQuote()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$quote = $this->checkoutSession->getQuote();
        $quotationQuote = $this->quotationSession->getQuote();
        if ($quotationQuote->getId()) {
            //TODO: Check why this is here and if this is suposed to check someting.
            $this->stockCheckHelper->isMoveToCartAllowed();
            $quote->merge($quotationQuote);
            $quote->collectTotals();
            $quote->save();
            $this->checkoutSession->setQuoteId($quote->getId());
            $quotationQuote->removeAllItems();
            $this->quoteRepository->save($quotationQuote);
        } else {
            throw new NoSuchEntityException(
                new \Magento\Framework\Phrase(
                    'This quote no longer exists.'
                )
            );
        }
        return $quote;
		}
	}
}
