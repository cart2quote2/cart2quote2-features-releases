<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\Quote\Section;
trait Provider
{
    /**
     * @var \Magento\Framework\EntityManager\EntityManager
     */
    /**
     * @var \Cart2Quote\Quotation\Model\Quote\SectionFactory
     */
    /**
     * @var \Cart2Quote\Quotation\Model\ResourceModel\Quote\Section
     */
    /**
     * @var \Cart2Quote\Quotation\Model\ResourceModel\Quote\Section\Collection
     */
    /**
     * Provider constructor.
     *
     * @param \Magento\Framework\EntityManager\EntityManager $entityManager
     * @param \Cart2Quote\Quotation\Model\ResourceModel\Quote\Section\Collection $sectionCollection
     * @param \Cart2Quote\Quotation\Model\Quote\SectionFactory $sectionFactory
     * @param \Cart2Quote\Quotation\Model\ResourceModel\Quote\Section $sectionResourceModel
     */
    /**
     * Get all sections for a given quote id
     *
     * @param int $quoteId
     * @return \Cart2Quote\Quotation\Api\Data\Quote\SectionInterface[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getSections($quoteId)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$sections = [];
        $ids = $this->sectionCollection->getSectionIdsForQuote($quoteId);
        foreach ($ids as $id) {
            $sections[] = $this->getSection($id);
        }
        return $sections;
		}
	}
    /**
     * Get section by id
     *
     * @param int $sectionId
     * @return \Cart2Quote\Quotation\Model\Quote\Section
     */
    private function getSection($sectionId)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$section = $this->sectionFactory->create();
        $this->sectionResourceModel->load($section, $sectionId);
        return $section;
		}
	}
}
