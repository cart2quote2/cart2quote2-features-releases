<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\Quote\Email;
use Cart2Quote\Quotation\Model\Quote\Email\Sender\QuoteSenderInterface;
/**
 * Trait AbstractSender
 *
 * @package Cart2Quote\Quotation\Model\Quote\Email
 */
trait AbstractSender
{
    /**
     * @var \Cart2Quote\Quotation\Model\Quote\Pdf\Quote
     */
    /**
     * @var \Magento\Sales\Model\Order\Email\SenderBuilderFactory
     */
    /**
     * @var \Magento\Sales\Model\Order\Email\Container\Template
     */
    /**
     * @var \Cart2Quote\Quotation\Model\Quote\Email\Container\IdentityInterface
     */
    /**
     * @var \Psr\Log\LoggerInterface
     */
    /**
     * @var \Cart2Quote\Quotation\Model\Quote\Address\Renderer
     */
    /**
     * AbstractSender constructor
     *
     * @param \Magento\Sales\Model\Order\Email\Container\Template $templateContainer
     * @param \Magento\Sales\Model\Order\Email\Container\IdentityInterface $identityContainer
     * @param \Cart2Quote\Quotation\Model\Quote\Email\SenderBuilderFactory $senderBuilderFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Cart2Quote\Quotation\Model\Quote\Address\Renderer $addressRenderer
     * @param \Cart2Quote\Quotation\Model\Quote\Pdf\Quote $pdfModel
     */
    /**
     * Send the mail if this mail is enabled
     *
     * @param \Cart2Quote\Quotation\Model\Quote $quote
     * @param array|null $attachments
     * @param mixed|null $internalEmail
     * @return bool
     */
    private function checkAndSend(
        \Cart2Quote\Quotation\Model\Quote $quote,
        $attachments = null,
        $internalEmail = null
    ) {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$this->identityContainer->setStore($quote->getStore());
        if (!$this->identityContainer->isEnabled()) {
            return false;
        }
        $this->prepareTemplate($quote, $internalEmail);
        /** @var SenderBuilder $sender */
        $sender = $this->getSender();
        try {
            $sender->send($attachments, $quote);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            return false;
        }
        try {
            $sender->sendCopyTo($attachments);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            return false;
        }
        return true;
		}
	}
    /**
     * Prepare template
     *
     * @param \Cart2Quote\Quotation\Model\Quote $quote
     * @param mixed|null $internalEmail
     * @return void
     */
    private function prepareTemplate(\Cart2Quote\Quotation\Model\Quote $quote, $internalEmail)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$this->templateContainer->setTemplateOptions($this->getTemplateOptions());
        $templateId = $this->identityContainer->getTemplateId();
        if (!$quote->getCustomerIsGuest()) {
            $customerName = $quote->getBillingAddress()->getName();
        } else {
            $customerName = $quote->getCustomerName();
        }
        if ($internalEmail) {
            $this->identityContainer->setCustomerName($quote->getEmailReceiverAttributes($quote->getUserId(), true));
            $this->identityContainer->setCustomerEmail($quote->getEmailReceiverAttributes($quote->getUserId(), false));
        } else {
            $this->identityContainer->setCustomerName($customerName);
            $this->identityContainer->setCustomerEmail($quote->getCustomerEmail());
        }
        $this->templateContainer->setTemplateId($templateId);
		}
	}
    /**
     * Get template options
     *
     * @return array
     */
    private function getTemplateOptions()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return [
            'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
            'store' => $this->identityContainer->getStore()->getStoreId()
        ];
		}
	}
    /**
     * Get sender object
     *
     * @return SenderBuilder|\Magento\Sales\Model\Order\Email\SenderBuilder
     */
    private function getSender()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return $this->senderBuilderFactory->create(
            [
                'templateContainer' => $this->templateContainer,
                'identityContainer' => $this->identityContainer,
            ]
        );
		}
	}
    /**
     * Get the shipping address formated (html)
     *
     * @param \Cart2Quote\Quotation\Model\Quote $quote
     * @return null|string
     */
    private function getFormattedShippingAddress(\Cart2Quote\Quotation\Model\Quote $quote)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return $quote->getIsVirtual()
            ? null
            : $this->addressRenderer->formatQuoteAddress($quote->getShippingAddress(), 'html');
		}
	}
    /**
     * Get the billing address formatted (html)
     *
     * @param \Cart2Quote\Quotation\Model\Quote $quote
     * @return null|string
     */
    private function getFormattedBillingAddress($quote)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return $this->addressRenderer->formatQuoteAddress($quote->getBillingAddress(), 'html');
		}
	}
}
