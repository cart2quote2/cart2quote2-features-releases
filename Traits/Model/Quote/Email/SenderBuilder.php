<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\Quote\Email;
use Zend_Mail_Exception;
trait SenderBuilder
{
    /**
     * @var \Magento\Sales\Model\Order\Email\Container\Template
     */
    /**
     * @var \Cart2Quote\Quotation\Model\Quote\Email\Container\IdentityInterface
     */
    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    /**
     * @var \Cart2Quote\Quotation\Model\Quote\Email\UploadTransportBuilder
     */
    /**
     * @var \Magento\Framework\Mail\Template\SenderResolverInterface
     */
    /**
     * @var \Magento\Framework\Module\Manager
     */
    /**
     * @var \Psr\Log\LoggerInterface
     */
    /**
     * @var \Magento\Framework\Filesystem\Io\File;
     */
    /**
     * SenderBuilder constructor
     *
     * @param UploadTransportBuilder $uploadTransportBuilder
     * @param \Magento\Framework\Mail\Template\SenderResolverInterface $senderResolver
     * @param \Magento\Sales\Model\Order\Email\Container\Template $templateContainer
     * @param \Magento\Sales\Model\Order\Email\Container\IdentityInterface $identityContainer
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Filesystem\Io\File $fileIo
     */
    /**
     * Prepare and send email message
     *
     * @param null|array $attachments
     * @param \Cart2Quote\Quotation\Model\Quote|null $quote
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\MailException
     */
    private function send(
        $attachments = null,
        $quote = null
    ) {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$this->transportBuilder = $this->uploadTransportBuilder;
        $attachedPart = $this->attachFiles($attachments);
        $this->configureEmailTemplate();
        $emailAvailable = false;
        if ($quote && $quote->getProposalEmailReceiver()) {
            $emailAvailable = true;
            $this->transportBuilder->addTo(
                $quote->getProposalEmailReceiver()
            );
        } elseif ($this->identityContainer->getRecieverEmail()) {
            $emailAvailable = true;
            $this->transportBuilder->addTo(
                $this->identityContainer->getRecieverEmail(),
                $this->identityContainer->getRecieverName()
            );
        }
        if ($quote && $quote->getProposalEmailCc()) {
            $this->transportBuilder->addCc(
                explode(';', $quote->getProposalEmailCc())
            );
        }
        if (!$emailAvailable) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('We can\'t send an email to a quote without an email address.')
            );
        }
        $copyTo = $this->identityContainer->getEmailCopyTo();
        if (!empty($copyTo) && $this->identityContainer->getCopyMethod() == 'bcc') {
            foreach ($copyTo as $email) {
                $this->transportBuilder->addBcc($email);
            }
        }
        $transport = $this->transportBuilder->getMessage($attachedPart);
        $this->transportBuilder->resetUploadTransportBuilder();
        $transport->sendMessage();
		}
	}
    /**
     * Prepare and send copy email message
     *
     * @param array $attachments
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\MailException
     */
    private function sendCopyTo(
        $attachments = null
    ) {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$copyTo = $this->identityContainer->getEmailCopyTo();
        if (!empty($copyTo) && $this->identityContainer->getCopyMethod() == 'copy') {
            $this->transportBuilder = $this->uploadTransportBuilder;
            $attachedPart = $this->attachFiles($attachments);
            $this->configureEmailTemplate();
            foreach ($copyTo as $email) {
                $this->transportBuilder->addTo($email);
            }
            $transport = $this->transportBuilder->getMessage($attachedPart);
            $this->transportBuilder->resetUploadTransportBuilder();
            $transport->sendMessage();
        }
		}
	}
    /**
     * Attach files to email message
     *
     * @param array $attachments
     * @return array
     */
    private function attachFiles($attachments)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$attachedPart = [];
        $isMagetrendEnabled = $this->moduleManager->isEnabled('Magetrend_PdfCart2Quote');
        if (!is_array($attachments)) {
            return $attachedPart;
        }
        foreach ($attachments as $attachmentName => $attachmentPath) {
            if ($this->fileIo->fileExists($attachmentPath)) {
                $attachedPart[] = $this->transportBuilder->attachFile($attachmentPath, $attachmentName);
            } elseif ($isMagetrendEnabled) {
                $attachmentPathParts = explode('//', $attachmentPath);
                if (is_array($attachmentPathParts) && isset($attachmentPathParts[1])) {
                    $magetrendAttachmentPath = '/' . $attachmentPathParts[1];
                    if ($this->fileIo->fileExists($magetrendAttachmentPath)) {
                        $attachedPart[] = $this->transportBuilder->attachFile(
                            $magetrendAttachmentPath,
                            $attachmentName
                        );
                    }
                }
            }
        }
        return $attachedPart;
		}
	}
    /**
     * Configure email template
     *
     * Fix for Magento not setting the email From header. (Fixed in M2.1.x, >M2.3.0 and >M2.2.8)
     *
     * @return void
     */
    private function configureEmailTemplate()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			parent::configureEmailTemplate();
        try {
            //setFromByScope only exists in the final fixed version of magento (>M2.3.0 and >M2.2.8))
            if (!method_exists(\Magento\Framework\Mail\Template\TransportBuilder::class, 'setFromByScope')) {
                $this->transportBuilder->setFrom($this->identityContainer->getEmailIdentity());
            }
        } catch (Zend_Mail_Exception $exception) {
            //catch 'From Header set twice' error
            //That would mean that is Magento 2.1.x where this isn't an issue
            $this->logger->error($exception->getMessage());
        }
		}
	}
}
