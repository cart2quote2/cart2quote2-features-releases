<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\Quote\Email\Sender;
trait QuoteProposalAcceptedSender
{
    /**
     * QuoteProposalAcceptedSender constructor
     *
     * @param \Magento\Sales\Model\Order\Email\Container\Template $templateContainer
     * @param \Magento\Sales\Model\Order\Email\Container\Container $identityContainer
     * @param \Cart2Quote\Quotation\Model\Quote\Email\SenderBuilderFactory $senderBuilderFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Cart2Quote\Quotation\Model\Quote\Address\Renderer $addressRenderer
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $globalConfig
     * @param \Cart2Quote\Quotation\Model\Quote\Pdf\Quote $pdfModel
     * @param \Cart2Quote\Quotation\Model\QuoteFactory $quoteFactory
     * @param \Magento\User\Model\UserFactory $userFactory
     * @param string $sendEmailIdentifier
     * @param string $emailSentIdentifier
     */
    // TODO: Fix UselessOverridingMethod
    // phpcs:disable Generic.CodeAnalysis.UselessOverridingMethod
    /**
     * Send function overwrite
     *
     * @param \Cart2Quote\Quotation\Model\Quote $quote
     * @param bool $forceSyncMode
     * @return bool
     */
    // TODO: Fix UselessOverridingMethod
    // phpcs:disable Generic.CodeAnalysis.UselessOverridingMethod
    private function send(\Cart2Quote\Quotation\Model\Quote $quote, $forceSyncMode = false)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return parent::send($quote, $forceSyncMode);
		}
	}
}
