<?php
namespace Cart2Quote\Features\Traits\Model\Quote\Address;
trait BillingAddressPersister
{
    /**
     * @var \Magento\Quote\Model\QuoteAddressValidator
     */
    /**
     * @var \Magento\Customer\Api\AddressRepositoryInterface
     */
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    /**
     * @var \Psr\Log\LoggerInterface
     */
    /**
     * BillingAddressPersister constructor.
     *
     * @param \Magento\Quote\Model\QuoteAddressValidator $addressValidator
     * @param \Magento\Customer\Api\AddressRepositoryInterface $addressRepository
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Psr\Log\LoggerInterface $_logger
     */
    /**
     * Save address for billing.
     *
     * @param \Magento\Quote\Api\Data\CartInterface $quote
     * @param \Magento\Quote\Api\Data\AddressInterface $address
     * @param bool $useForShipping
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\InputException
     */
    private function save(
        \Magento\Quote\Api\Data\CartInterface $quote,
        \Magento\Quote\Api\Data\AddressInterface $address,
        $useForShipping = false
    ) {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			/** @var \Magento\Quote\Model\Quote $quote */
        $this->addressValidator->validateForCart($quote, $address);
        $customerAddressId = $address->getCustomerAddressId();
        $shippingAddress = null;
        $addressData = [];
        if ($useForShipping) {
            $shippingAddress = $address;
        }
        $saveInAddressBook = $address->getSaveInAddressBook() ? 1 : 0;
        // Don't import customer address data when request path is quotation/quote/create
        if ($customerAddressId && str_contains($this->request->getPathInfo(), 'quotation/quote/create')) {
            try {
                $addressData = $this->addressRepository->getById($customerAddressId);
            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                $this->_logger->error('Error saving billing address: ' . $e->getMessage());
            }
            $address = $quote->getBillingAddress()->importCustomerAddressData($addressData);
            if ($useForShipping) {
                $shippingAddress = $quote->getShippingAddress()->importCustomerAddressData($addressData);
                $shippingAddress->setSaveInAddressBook($saveInAddressBook);
            }
        } elseif ($quote->getCustomerId()) {
            $address->setEmail($quote->getCustomerEmail());
        }
        $address->setSaveInAddressBook($saveInAddressBook);
        $quote->setBillingAddress($address);
        if ($useForShipping) {
            $shippingAddress->setSameAsBilling(1);
            $shippingAddress->setCollectShippingRates(true);
            $quote->setShippingAddress($shippingAddress);
        }
		}
	}
}
