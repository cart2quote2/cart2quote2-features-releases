<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\Quote\Pdf\Config;
use Magento\Framework\Config\SchemaLocatorInterface;
use Magento\Sales\Model\Order\Pdf\Config\SchemaLocator as MageSchemaLocator;
/**
 * Trait SchemaLocator
 * - Attributes config schema locator
 *
 */
trait SchemaLocator
{
    /**
     * Path to corresponding XSD file with validation rules for merged configs
     *
     * @var string
     */
    /**
     * Path to corresponding XSD file with validation rules for individual configs
     *
     * @var string
     */
    /**
     * @param \Magento\Framework\Module\Dir\Reader $moduleReader
     */
    /**
     * Get path to merged config schema
     */
    private function getSchema()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return $this->_schema;
		}
	}
    /**
     * Get path to per file validation schema
     */
    private function getPerFileSchema()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return $this->_schemaFile;
		}
	}
}
