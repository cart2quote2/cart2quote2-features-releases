<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\Quote\Pdf\Items;
use Imagick;
trait QuoteItem
{
    /**
     * Directory structure for Product Images
     */
    /**
     * Feed distance for Item Price
     */
    /**
     * Feed distance Item Qty
     */
    /**
     * Feed distance for Item Tax
     */
    /**
     * Feed distance Item Row Total
     */
    /**
     * Path to pdf_save_space in system.xml
     */
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    /**
     * @var \Cart2Quote\Quotation\Helper\Data
     */
    /**
     * @var \Cart2Quote\Quotation\Helper\ProductThumbnail
     */
    /**
     * @var \Cart2Quote\Quotation\Helper\CustomProduct
     */
    /**
     * QuoteItem constructor.
     *
     * @param \Cart2Quote\Quotation\Helper\CustomProduct $customProductHelper
     * @param \Cart2Quote\Quotation\Helper\QuotationTaxHelper $quotationTaxHelper
     * @param \Magento\Framework\Filesystem\Io\File $fileIo
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Tax\Helper\Data $taxData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Framework\Filter\FilterManager $filterManager
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface
     * @param \Cart2Quote\Quotation\Helper\Data $cart2QuoteHelper
     * @param \Cart2Quote\Quotation\Helper\ProductThumbnail $thumbnailHelper
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    /**
     * Draw item line
     *
     * @return void
     */
    private function draw()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$quote = $this->getQuote();
        $item = $this->getItem();
        $pdf = $this->getPdf();
        $page = $this->getPage();
        $this->_setFontRegular();
        $drawItems = [];
        $line = [];
        $feed = 30;
        $split = 35;
        $skuFeed = 205;
        $isCustomProduct = $this->customProductHelper->isCustomProduct($item);
        //Print Thumbnails Next to Product Name if enabled
        if ($this->thumbnailHelper->showProductThumbnailPdf()) {
            $this->drawProductImage($item, $pdf, $page, $feed, $skuFeed, $split);
            //reserve image space
            $line[] = [
                'text' => '',
                'feed' => $feed + 0,
                'addToLeft' => 0,
                'addToTop' => (40 - 10),
                'font' => 'italic',
                'isProductLine' => true
            ];
        }
        /* in case Product name is longer than 80 chars - it is written in a few lines */
        $product =  $this->productRepositoryInterface->getById(
            $item->getProduct()->getId(),
            false,
            $quote->getStoreId()
        );
        if ($isCustomProduct) {
            $name = $this->customProductHelper->getCustomProductName($item);
        } else {
            $name = $product->getName();
        }
        $name = htmlspecialchars_decode($name);
        /* optional items */
        if ($item->getCurrentTierItem()->getMakeOptional()) {
            $name .= '*';
        }
        $nameArray['font'] = 'bold';
        $nameArray['text'] = $pdf->getStringUtils()->split($name, $split, true, true);
        $nameArray['feed'] = $feed;
        $nameArray['addToTop'] = -5;
        $nameArray['isProductLine'] = true;
        $line[] = $nameArray;
        $nameLineCount = count($nameArray['text']);
        $enabledShortDescription = $this->_scopeConfig->getValue(
            'quotation_advanced/pdf/pdf_enable_short_description',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $splitDescription = [];
        $shortDescriptionSplit = 35;
        if ($enabledShortDescription) {
            /* set short description beneath name*/
            $product = $this->productRepositoryInterface->getById($item->getProductId());
            $shortDescription = $product->getShortDescription();
            if ($nameLineCount > 1) {
                $shortDescription = ' ';
            }
            if ($shortDescription != '' && $shortDescription != null) {
                $shortDescription = strip_tags(trim($shortDescription));
            }
            $splitDescription = $pdf->getStringUtils()->split($shortDescription, $shortDescriptionSplit, true, true);
            $line[] = [
                'text' => $splitDescription,
                'feed' => $feed + 5,
                'addToLeft' => 5,
                'addToTop' => 5,
                'font' => 'italic',
                'isProductLine' => true
            ];
        }
        // draw SKUs
        $text = [];
        $skuSplit = 14;
        $skuLineCount = count($pdf->getStringUtils()->split($item->getSku(), $skuSplit));
        foreach ($pdf->getStringUtils()->split($item->getSku(), $skuSplit) as $part) {
            if ($isCustomProduct) {
                $text[] = $this->customProductHelper->getCustomProductSku($item);
            } else {
                $text[] = $part;
            }
        }
        $line[] = ['text' => $text, 'feed' => $skuFeed, 'isProductLine' => true, 'addToTop' => -5];
        // draw prices
        $line = $this->drawProductPrices($item, $quote, $line);
        /**
         * Draw Options
         */
        $optionsSplitWidth = 145;
        $options = $this->getQuoteItemOptions();
        if ($options) {
            foreach ($options as $option) {
                $label = $option['label'];
                $optionValue = strip_tags($option['value']);
                $optionValues = $this->splitTextOnWidth($optionValue, $optionsSplitWidth);
                if (empty($optionValues) || count($optionValues) == 1) {
                    $customOption = sprintf('%s: %s', $label, $optionValue);
                    $customOption = str_replace('::', ':', $customOption);
                    $customOptionParts = $this->splitTextOnWidth($customOption, $optionsSplitWidth);
                    if (count($customOptionParts) == 1) {
                        $optionText[] = $customOption;
                    } else {
                        $customOption = sprintf('%s:', $label);
                        $customOption = str_replace('::', ':', $customOption);
                        $optionText[] = $customOption;
                        if (isset($optionValues[0])) {
                            $optionText[] = $optionValues[0];
                        }
                    }
                } else {
                    foreach ($optionValues as $key => $optionValue) {
                        if ($key == 0) {
                            $customOption = sprintf('%s:', $label);
                            $customOption = str_replace('::', ':', $customOption);
                            $optionText[] = $customOption;
                        }
                        $optionText[] = ' ' . $optionValue;
                    }
                }
            }
            $top = (count($splitDescription) + max($nameLineCount, $skuLineCount)) * 10;
            $line[] = [
                'text' => $optionText,
                'feed' => $feed + 5,
                'isProductLine' => true,
                'addToTop' => $top,
                'font' => 'italic'
            ];
        }
        /**
         * Print Bundle Options
         */
        if ($item->getProductType() == 'bundle') {
            $longestChildLine = 0;
            $longestChildLineWidth = 0;
            foreach ($item->getChildren() as $child) {
                $childProduct = $this->productRepositoryInterface->getById(
                    $child->getProductId(),
                    false,
                    $quote->getStoreId()
                );
                $childName = $childProduct->getName();
                $childQty = $child->getQty();
                $childLine = sprintf('%s x %s', $childName, $childQty);
                $longestChildLine = max($longestChildLine, $pdf->getStringUtils()->strlen($childLine));
                $longestChildLineWidth = max($longestChildLineWidth, $this->getTextWidth($childLine));
                $bundleText[] = $childLine;
            }
            if ($longestChildLineWidth < 85) {
                $top = (count($splitDescription) + $nameLineCount) * 10;
            } else {
                $top = (count($splitDescription) + max($nameLineCount, $skuLineCount)) * 10;
            }
            $line[] = [
                'text' => $bundleText,
                'feed' => $feed + 5,
                'isProductLine' => true,
                'addToTop' => $top,
                'font' => 'italic'
            ];
        }
        $optionLineCount = 0;
        if (isset($optionText)) {
            $optionLineCount += count($optionText);
        }
        if (isset($bundleText)) {
            $optionLineCount += count($bundleText);
        }
        //draw comment
        $comment = '';
        if ($item->getDescription()) {
            $comment = $item->getDescription();
            $top = (count($splitDescription) + max(($nameLineCount + $optionLineCount), $skuLineCount)) * 10;
            if (isset($longestChildLine)) {
                if ($longestChildLine >= 85) {
                    $top += (count($splitDescription) + max($nameLineCount, $skuLineCount)) * 10;
                    $top -= 10;
                }
            }
            //header
            $line[] = [
                'text' => $pdf->getStringUtils()->split(__('Comment'), 35, true, true),
                'feed' => $feed + 0,
                'addToLeft' => 0,
                'addToTop' => $top + 5,
                'font' => 'bold',
                'isProductLine' => true
            ];
            //comment
            //$splitComment = $pdf->getStringUtils()->split($comment, 35, true, true);
            $splitComment = $this->splitTextOnWidth($comment, $optionsSplitWidth);
            $line[] = [
                'text' => $splitComment,
                'feed' => $feed + 5,
                'addToLeft' => 0,
                'addToTop' => $top + 15,
                'font' => 'italic',
                'isProductLine' => true
            ];
        }
        $drawItems[]['lines'][] = $line;
        //always add some empty space
        $lines[][] = [
            'text' => [''],
            'feed' => 0,
            'addToTop' => 5
        ];
        $drawItems[] = ['lines' => $lines, 'height' => 5];
        /**
         * Print all lines on the page
         */
        $page = $pdf->drawLineBlocks($page, $drawItems, ['table_header' => true]);
        $this->setPage($page);
		}
	}
    /**
     * Get the quote item options
     *
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getQuoteItemOptions()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$result = [];
        $item = $this->getItem();
        $keys = ['options', 'additional_options', 'attributes_info'];
        $product = $item->getProduct();
        if (isset($product)) {
            $options = $product->getTypeInstance()->getOrderOptions($product);
            foreach ($keys as $key) {
                if (isset($options[$key])) {
                    $result += $options[$key];
                }
            }
        }
        return $result;
		}
	}
    /**
     * Set the tier quantity to PDF
     *
     * @param \Cart2Quote\Quotation\Model\Quote $quote
     * @param \Cart2Quote\Quotation\Model\Quote\TierItem $item
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function setTierItemsPdf($quote, $item)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$fontType = 'italic'; //differentiate selected from options
        $prices = $this->getQuoteTierItemPricesForDisplay();
        $tierItems = $item->getTierItems();
        $addToTop = 15;
        $line = [];
        $bottomMargin = 15;
        $extraPriceFeed = 0;
        if ($this->_taxData->displaySalesBothPrices()) {
            $bottomMargin = 0;
            $extraPriceFeed = 20;
        }
        foreach ($tierItems->getItems() as $tierItem) {
            if ($tierItem->getQty() != $item->getQty()) {
                $tax = $quote->formatPriceTxt($tierItem->getTaxAmount());
                $addToTop += 15;
                $tierPrices = $prices[$tierItem->getId()];
                foreach ($tierPrices as $priceData) {
                    // draw Price
                    $line[] = [
                        'text' => $priceData['label'] . $priceData['price'],
                        'feed' => $this::PRICE_FEED + $extraPriceFeed,
                        'font' => $fontType,
                        'align' => 'right',
                        'isProductLine' => true,
                        'addToTop' => $addToTop - $bottomMargin
                    ];
                    // draw Subtotal
                    $line[] = [
                        'text' => $priceData['subtotal'],
                        'feed' => $this::ROW_TOTAL_FEED,
                        'font' => $fontType,
                        'align' => 'right',
                        'isProductLine' => true,
                        'addToTop' => $addToTop - $bottomMargin
                    ];
                    $addToTop += 15;
                }
                $line[] = [
                    'text' => $tierItem->getQty() * 1,
                    'feed' => $this->getProductRowFeed('qty'),
                    'font' => $fontType,
                    'isProductLine' => true,
                    'addToTop' => $addToTop - 30
                ];
                if ($this->cart2QuoteHelper->displayTaxInPDF()) {
                    $line[] = [
                        'text' => $tax,
                        'feed' => $this::TAX_FEED,
                        'font' => $fontType,
                        'align' => 'right',
                        'isProductLine' => true,
                        'addToTop' => $addToTop - 30
                    ];
                }
            }
        }
        return $line;
		}
	}
    /**
     * Print Thumbnails Next to Product Name if enabled
     *
     * @param \Magento\Framework\DataObject $item
     * @param \Magento\Sales\Model\Order\Pdf\AbstractPdf $pdf
     * @param \Zend_Pdf_Page $page
     * @param int $feed
     * @param int $skuFeed
     * @param int $split
     * @return bool
     * @throws \Zend_Pdf_Exception
     */
    private function drawProductImage(
        \Magento\Framework\DataObject $item,
        \Magento\Sales\Model\Order\Pdf\AbstractPdf $pdf,
        \Zend_Pdf_Page $page,
        &$feed,
        &$skuFeed,
        &$split
    ) {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			if ($productThumbs = $this->thumbnailHelper->showProductThumbnailPdf()) {
            $feed = 70;
            $skuFeed = 210;
            $split = 30;
            if ($thumbnail = $item->getProduct()->getThumbnail()) {
                $widthLimit = 40;
                $heightLimit = 40;
                $imagePath = self::CATALOG_PRODUCT_PATH . $thumbnail;
                if ($this->_mediaDirectory->isFile($imagePath)) {
                    $fullImagePath = $this->_mediaDirectory->getAbsolutePath($imagePath);
                    //Zend_Pdf_Image::imageWithPath supports: tif, tiff, png, jpg, jpe, and jpeg support
                    $supportedImageTypes = [
                        'tif',
                        'TIF',
                        'tiff',
                        'TIFF',
                        'png',
                        'PNG',
                        'jpg',
                        'JPG',
                        'jpe',
                        'JPE',
                        'jpeg',
                        'JPEG'
                    ];
                    $convertableImageTypes = ['gif', 'GIF', 'bmp', 'BMP'];
                    $imageType = $this->getImageType($fullImagePath);
                    if (in_array($imageType, $supportedImageTypes)) {
                        $image = \Zend_Pdf_Image::imageWithPath($fullImagePath);
                    } elseif (in_array($imageType, $convertableImageTypes)) {
                        $source = new Imagick($fullImagePath);
                        if (isset($source)) {
                            $source->setImageFormat('jpeg');
                            $imagePath = sys_get_temp_dir() . '/' . sha1(rand() . time()) . '.jpg';
                            $source->writeImage($imagePath);
                            $source->destroy();
                            $image = \Zend_Pdf_Image::imageWithPath($imagePath);
                        }
                    }
                    //return if no image is generated
                    if (!isset($image)) {
                        return $thumbnail;
                    }
                    //assuming the image is not a "skyscraper"
                    $width = $image->getPixelWidth();
                    $height = $image->getPixelHeight();
                    //preserving aspect ratio (proportions)
                    $ratio = $width / $height;
                    if ($ratio > 1 && $width > $widthLimit) {
                        $width = $widthLimit;
                        $height = $width / $ratio;
                    } elseif ($ratio < 1 && $height > $heightLimit) {
                        $height = $heightLimit;
                        $width = $height * $ratio;
                    } elseif ($ratio == 1 && $height > $heightLimit) {
                        $height = $heightLimit;
                        $width = $widthLimit;
                    }
                    $top = $pdf->y;
                    if ($top > 840) {
                        $top = 830;
                    }
                    $x1 = 25;
                    $x2 = $x1 + $width;
                    $y1 = $top - $height;
                    $y2 = $top;
                    //coordinates after transformation are rounded by Zend
                    $page->drawImage($image, $x1, $y1, $x2, $y2);
                }
            }
        }
        return $productThumbs;
		}
	}
    /**
     * Get product row feed
     *
     * @param string $feedType
     * @return int
     */
    private function getProductRowFeed(string $feedType)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			if ($feedType !== 'qty') {
            return 0;
        }
        return $this->cart2QuoteHelper->displayTaxInPDF() ? $this::QTY_FEED : $this::QTY_FEED + 30;
		}
	}
    /**
     * Draw product prices
     *
     * @param \Magento\Framework\DataObject $item
     * @param \Cart2Quote\Quotation\Model\Quote $quote
     * @param array $line
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function drawProductPrices(
        \Magento\Framework\DataObject $item,
        \Cart2Quote\Quotation\Model\Quote $quote,
        array $line
    ) {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$fontType = 'bold';
        $qty = $item->getQty();
        $prices = $this->getQuoteTierItemPricesForDisplay();
        $itemPrices = $prices[$item->getCurrentTierItem()->getId()];
        $extraPriceFeed = 0;
        $extraDiscountFeed = 55;
        if ($this->_taxData->displaySalesBothPrices()) {
            $extraPriceFeed = 20;
        }
        if ($item->getParentItem()) {
            $fontType = 'regular';
            $tax = null;
            $row_total = null;
            $qty = null;
        } else {
            $tax = $quote->formatPriceTxt($item->getTaxAmount());
        }
        $i = -5;
        foreach ($itemPrices as $priceData) {
            // draw Price
            $line[] = [
                'text' => $priceData['label'] . ' ' . $priceData['price'],
                'feed' => $this::PRICE_FEED + $extraPriceFeed,
                'font' => 'bold',
                'align' => 'right',
                'isProductLine' => true,
                'addToTop' => $i
            ];
            if ($item->getDiscountAmount() > 0 && $this->cart2QuoteHelper->showQuoteAdjustment() !== false) {
                $line[] = [
                    'text' => $priceData['discount'],
                    'feed' => $this::PRICE_FEED + $extraDiscountFeed,
                    'font' => $fontType,
                    'align' => 'right',
                    'isProductLine' => true,
                    'addToTop' => -5
                ];
            }
            // draw Subtotal
            $line[] = [
                'text' => $priceData['subtotal'],
                'feed' => $this::ROW_TOTAL_FEED,
                'font' => 'bold',
                'align' => 'right',
                'isProductLine' => true,
                'addToTop' => $i
            ];
            $i += 15;
        }
        if ($item->getCurrentTierItem()->getCustomPrice() != null) {
            $line[] = [
                'text' => $qty,
                'feed' => $this->getProductRowFeed('qty'),
                'font' => $fontType,
                'isProductLine' => true,
                'addToTop' => -5
            ];
            if ($this->cart2QuoteHelper->displayTaxInPDF()) {
                $line[] = [
                    'text' => $tax,
                    'feed' => $this::TAX_FEED,
                    'font' => $fontType,
                    'align' => 'right',
                    'isProductLine' => true,
                    'addToTop' => -5
                ];
            }
            if ($item->getTierItems()) {
                $lineTier = $this->setTierItemsPdf($quote, $item);
                $line = array_merge($line, $lineTier);
            }
        }
        return $line;
		}
	}
    /**
     * Get the setting to save fonts
     *
     * @return bool
     */
    private function saveSpaceFonts()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return (boolean)$this->_scopeConfig->getValue(
            self::XML_PATH_SAVE_SPACE_FONTS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
		}
	}
    /**
     * Set font as regular
     *
     * @param  int $size
     * @return \Zend_Pdf_Resource_Font
     */
    private function _setFontRegular($size = 7)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			if ($this->saveSpaceFonts()) {
            $font = \Zend_Pdf_Font::fontWithName(
                \Zend_Pdf_Font::FONT_TIMES,
                \Zend_Pdf_Font::EMBED_DONT_EMBED
            );
        } else {
            $fontPath = $this->_rootDirectory->getAbsolutePath('lib/internal/GnuFreeFont/FreeSerif.ttf');
            //fallback for <M2.3
            if (!$this->fileIo->fileExists($fontPath)) {
                $fontPath = $this->_rootDirectory
                    ->getAbsolutePath('lib/internal/LinLibertineFont/LinLibertine_Re-4.4.1.ttf');
            }
            $font = \Zend_Pdf_Font::fontWithPath($fontPath);
        }
        $this->getPage()->setFont($font, $size);
        return $font;
		}
	}
    /**
     * Set font as bold
     *
     * @param  int $size
     * @return \Zend_Pdf_Resource_Font
     */
    private function _setFontBold($size = 7)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			if ($this->saveSpaceFonts()) {
            $font = \Zend_Pdf_Font::fontWithName(
                \Zend_Pdf_Font::FONT_TIMES_BOLD,
                \Zend_Pdf_Font::EMBED_DONT_EMBED
            );
        } else {
            $fontPath = $this->_rootDirectory->getAbsolutePath('lib/internal/GnuFreeFont/FreeSerifBold.ttf');
            //fallback for <M2.3
            if (!$this->fileIo->fileExists($fontPath)) {
                $fontPath = $this->_rootDirectory
                    ->getAbsolutePath('lib/internal/LinLibertineFont/LinLibertine_Bd-2.8.1.ttf');
            }
            $font = \Zend_Pdf_Font::fontWithPath($fontPath);
        }
        $this->getPage()->setFont($font, $size);
        return $font;
		}
	}
    /**
     * Set font as italic
     *
     * @param  int $size
     * @return \Zend_Pdf_Resource_Font
     */
    private function _setFontItalic($size = 7)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			if ($this->saveSpaceFonts()) {
            $font = \Zend_Pdf_Font::fontWithName(
                \Zend_Pdf_Font::FONT_TIMES_ITALIC,
                \Zend_Pdf_Font::EMBED_DONT_EMBED
            );
        } else {
            $fontPath = $this->_rootDirectory->getAbsolutePath('lib/internal/GnuFreeFont/FreeSerifItalic.ttf');
            //fallback for <M2.3
            if (!$this->fileIo->fileExists($fontPath)) {
                $fontPath = $this->_rootDirectory
                    ->getAbsolutePath('lib/internal/LinLibertineFont/LinLibertine_It-2.8.2.ttf');
            }
            $font = \Zend_Pdf_Font::fontWithPath($fontPath);
        }
        $this->getPage()->setFont($font, $size);
        return $font;
		}
	}
}
