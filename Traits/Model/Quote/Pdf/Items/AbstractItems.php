<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\Quote\Pdf\Items;
/**
 * Quote Pdf Items renderer Abstract
 */
trait AbstractItems
{
    /**
     * Core string
     *
     * @var \Magento\Framework\Stdlib\StringUtils
     */
    /**
     * @var \Cart2Quote\Quotation\Model\Quote
     */
    /**
     * @var \Cart2Quote\Quotation\Helper\QuotationTaxHelper
     */
    /**
     * @var \Magento\Framework\Filesystem\Io\File;
     */
    /**
     * AbstractItems constructor.
     *
     * @param \Cart2Quote\Quotation\Helper\Data $cart2QuoteHelper
     * @param \Cart2Quote\Quotation\Helper\QuotationTaxHelper $quotationTaxHelper
     * @param \Magento\Framework\Filesystem\Io\File $fileIo
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Tax\Helper\Data $taxData
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Framework\Filter\FilterManager $filterManager
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    /**
     * Set Quote model
     *
     * @param \Cart2Quote\Quotation\Model\Quote $quote
     * @return $this
     */
    private function setQuote(\Cart2Quote\Quotation\Model\Quote $quote)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$this->quote = $quote;
        return $this;
		}
	}
    /**
     * Retrieve quote object
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return \Cart2Quote\Quotation\Model\Quote
     */
    private function getQuote()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			if (null === $this->quote) {
            throw new \Magento\Framework\Exception\LocalizedException(__('The quote object is not specified.'));
        }
        return $this->quote;
		}
	}
    /**
     * Get the tier item prices to display
     *
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getQuoteTierItemPricesForDisplay()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$quote = $this->getQuote();
        $item = $this->getItem();
        $tierItems = $item->getTierItems();
        $prices = [];
        foreach ($tierItems as $item) {
            if ($item->getDiscountAmount() > 0 && $this->cart2QuoteHelper->showQuoteAdjustment() !== false) {
                $discount = '- ' . $quote->formatPriceTxt($item->getDiscountAmount());
            } else {
                $discount = null;
            }
            if ($this->_taxData->displaySalesBothPrices()) {
                $prices[$item->getId()] = [
                    [
                        'label' => __('Excl. Tax') . ':',
                        'price' => $quote->formatPriceTxt($this->quotationTaxHelper->getPriceExclTax($item)),
                        'subtotal' => $quote->formatPriceTxt($item->getRowTotal() - $item->getDiscountAmount()),
                        'discount' => $discount
                    ],
                    [
                        'label' => __('Incl. Tax') . ':',
                        'price' => $quote->formatPriceTxt($item->getPriceInclTax()),
                        'subtotal' => $quote->formatPriceTxt($item->getRowTotalInclTax() - $item->getDiscountAmount()),
                        'discount' => $discount
                    ],
                ];
            } elseif ($this->_taxData->displaySalesPriceInclTax()) {
                $prices[$item->getId()] = [
                    [
                        'label' => '',
                        'price' => $quote->formatPriceTxt($item->getPriceInclTax()),
                        'subtotal' => $quote->formatPriceTxt($item->getRowTotalInclTax() - $item->getDiscountAmount()),
                        'discount' => $discount
                    ],
                ];
            } else {
                $prices[$item->getId()] = [
                    [
                        'label' => '',
                        'price' => $quote->formatPriceTxt($this->quotationTaxHelper->getPriceExclTax($item)),
                        'subtotal' => $quote->formatPriceTxt($item->getRowTotal() - $item->getDiscountAmount()),
                        'discount' => $discount
                    ],
                ];
            }
        }
        return $prices;
		}
	}
    /**
     * Function that calculates the width of a string
     *
     * @param string $string
     * @return float|int
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getTextWidth($string)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return $this->getPdf()->widthForStringUsingFontSize(
            $string,
            $this->getPage()->getFont(),
            $this->getPage()->getFontSize()
        );
		}
	}
    /**
     * Function that splits text on width
     *
     * @param string $string
     * @param int $width
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function splitTextOnWidth($string, $width)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return $this->getPdf()->splitTextOnWidth(
            $string,
            $this->getPage()->getFont(),
            $this->getPage()->getFontSize(),
            $width
        );
		}
	}
    /**
     * Get image type
     *
     * @param string $imagePath
     * @return string filetype
     */
    private function getImageType($imagePath)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			if ($this->fileIo->fileExists($imagePath)) {
            if (function_exists('exif_imagetype')) {
                //Check actual file type
                $imageType = exif_imagetype($imagePath);
                if ($imageType == IMAGETYPE_GIF) {
                    return 'gif';
                } elseif ($imageType == IMAGETYPE_JPEG) {
                    return 'jpeg';
                } elseif ($imageType == IMAGETYPE_PNG) {
                    return 'png';
                } elseif ($imageType == IMAGETYPE_SWF) {
                    return 'swf';
                } elseif ($imageType == IMAGETYPE_PSD) {
                    return 'psd';
                } elseif ($imageType == IMAGETYPE_BMP) {
                    return 'bmp';
                } elseif ($imageType == IMAGETYPE_TIFF_II) {
                    return 'tiff';
                } elseif ($imageType == IMAGETYPE_TIFF_MM) {
                    return 'tiff';
                } elseif ($imageType == IMAGETYPE_ICO) {
                    return 'ico';
                } else {
                    return 'Unsupported file type';
                }
            } else {
                //Check file type based on extension
                $ext = strtoupper($this->fileIo->getPathInfo($imagePath)['extension']);
                $extensions = ['gif', 'jpeg', 'jpg', 'jpe', 'png', 'swf', 'psd', 'bmp', 'tiff', 'tif', 'ico'];
                if (in_array(strtolower($ext), $extensions)) {
                    return $ext;
                } else {
                    return 'Unsupported file type';
                }
            }
        }
        return 'File does not exist';
		}
	}
}
