<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\Quote\Pdf\Total;
trait Tax
{
    /**
     * @var \Cart2Quote\Quotation\Helper\Data
     */
    /**
     * @var \Magento\Tax\Model\Config
     */
    /**
     * @var \Magento\Tax\Helper\Data
     */
    /**
     * @param \Cart2Quote\Quotation\Helper\Data $helper
     * @param \Magento\Tax\Helper\Data $taxHelper
     * @param \Magento\Tax\Model\Calculation $taxCalculation
     * @param \Magento\Tax\Model\ResourceModel\Sales\Order\Tax\CollectionFactory $ordersFactory
     * @param \Magento\Tax\Model\Config $taxConfig
     * @param array $data
     */
    /**
     * Get tax amount for display on PDF
     *
     * @return array
     */
    private function getTotalsForDisplay()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$totals = [];
        if ($this->helper->displayTaxInPDF()) {
            $quote = $this->getQuote();
            $totals = [];
            $store = $this->getSource()->getStore();
            if ($this->_taxConfig->displaySalesTaxWithGrandTotal($store)) {
                return [];
            }
            $tax = 0;
            foreach ($quote->getAllVisibleItems() as $item) {
                $tax = $tax + $item->getTaxAmount();
            }
            //add shipping tax
            if (!empty($this->getShippingAddress()) && $this->getShippingAddress()->getShippingTaxAmount()) {
                $tax = $tax + $this->getShippingAddress()->getShippingTaxAmount();
            }
            if ($this->_taxConfig->displaySalesFullSummary($store)) {
                $totals = $this->getFullTaxInfo();
            }
            $tax = $quote->formatPriceTxt($tax);
            $totals = array_merge($totals, parent::getTotalsForDisplay());
            $totals[0]['amount'] = $tax;
        }
        return $totals;
		}
	}
}
