<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\Quote\Pdf\Total;
trait Discount
{
    /**
     * @var \Magento\Tax\Model\Config
     */
    /**
     * @var \Cart2Quote\Quotation\Model\Quote
     */
    /**
     * @var \Magento\Tax\Helper\Data
     */
    /**
     * @param \Magento\Tax\Helper\Data $taxHelper
     * @param \Magento\Tax\Model\Calculation $taxCalculation
     * @param \Magento\Tax\Model\ResourceModel\Sales\Order\Tax\CollectionFactory $ordersFactory
     * @param \Magento\Tax\Model\Config $taxConfig
     * @param array $data
     */
    /**
     * Get the underscore cache
     *
     * TODO: no used function?
     *
     * @return array
     */
    private function getUnderscoreCache()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return self::$_underscoreCache;
		}
	}
    /**
     * Get the tax config
     *
     * @return \Magento\Tax\Model\Config
     */
    private function getTaxConfig()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return $this->_taxConfig;
		}
	}
    /**
     * Get tax calculation
     *
     * @return \Magento\Tax\Model\Calculation
     */
    private function getTaxCalculation()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return $this->_taxCalculation;
		}
	}
    /**
     * Get tax orders factory
     *
     * @return \Magento\Tax\Model\ResourceModel\Sales\Order\Tax\CollectionFactory
     */
    private function getTaxOrdersFactory()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return $this->_taxOrdersFactory;
		}
	}
    /**
     * Get quote
     *
     * @return mixed
     */
    private function getQuote()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return $this->_quote;
		}
	}
    /**
     * Get the tax helper
     *
     * @return \Magento\Tax\Helper\Data
     */
    private function getTaxHelper()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return $this->_taxHelper;
		}
	}
    /**
     * Get discounts for display on PDF
     *
     * @return array
     */
    private function getTotalsForDisplay()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$totals = [];
        $amount = 0;
        $quote = $this->getSource();
        $couponCode = $quote->getCouponCode();
        $title = __($this->getTitle());
        $label = null;
        if ($couponCode != '') {
            $label = $title . ' (' . $couponCode . '):';
            $amount = $this->getAmount();
        }
        $totals = array_merge($totals, parent::getTotalsForDisplay());
        //being a discount we want the outcome to be negative
        if ($amount > 0) {
            $amount = $amount - ($amount * 2);
        }
        if ($label != null) {
            $totals[0]['amount'] = $quote->formatPriceTxt($amount);
            $totals[0]['label'] = $label;
        }
        return $totals;
		}
	}
    /**
     * Get the total
     *
     * @return float
     */
    private function getAmount()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$subtotalWithDiscount = $this->getSource()->getSubtotalWithDiscount();
        $subtotal = $this->getSource()->getSubtotal();
        $diffAmount    = $this->getSource()->getGrandTotal() - $subtotal;
        $totalDiscount = $subtotalWithDiscount - $this->getSource()->getOriginalSubtotal();
        $amount = $subtotalWithDiscount - $subtotal;
        if ($diffAmount == $totalDiscount) {
            $items  = $this->getSource()->getAllVisibleItems();
            foreach ($items as $item) {
                if (! empty($item->getWeeeTaxAppliedAmount())) {
                    $weeeTax[] = $item->getWeeeTaxAppliedAmount();
                }
            }
            if (!empty($weeeTax)) {
                $weeeTaxTotal = array_sum($weeeTax);
                $amount = $amount + $weeeTaxTotal;
                return $amount;
            }
        } else {
            return $subtotalWithDiscount - $subtotal;
        }
		}
	}
}
