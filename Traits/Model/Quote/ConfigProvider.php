<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\Quote;
trait ConfigProvider
{
    /**
     * Replace the checkout session with the quote session.
     *
     * @param \Cart2Quote\Quotation\Model\Session $quoteSession
     * @param \Magento\Checkout\Helper\Data $checkoutHelper
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Customer\Model\Url $customerUrlManager
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Magento\Quote\Api\CartItemRepositoryInterface $quoteItemRepository
     * @param \Magento\Quote\Api\ShippingMethodManagementInterface $shippingMethodManager
     * @param \Magento\Catalog\Helper\Product\ConfigurationPool $configurationPool
     * @param \Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory
     * @param \Magento\Framework\Locale\FormatInterface $localeFormat
     * @param \Magento\Customer\Model\Address\Mapper $addressMapper
     * @param \Magento\Customer\Model\Address\Config $addressConfig
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param \Magento\Catalog\Helper\Image $imageHelper
     * @param \Magento\Framework\View\ConfigInterface $viewConfig
     * @param \Magento\Directory\Model\Country\Postcode\ConfigInterface $postCodesConfig
     * @param \Magento\Checkout\Model\Cart\ImageProvider $imageProvider
     * @param \Magento\Directory\Helper\Data $directoryHelper
     * @param \Magento\Quote\Api\CartTotalRepositoryInterface $cartTotalRepository
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Shipping\Model\Config $shippingMethodConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Quote\Api\PaymentMethodManagementInterface $paymentMethodManagement
     * @param \Magento\Framework\UrlInterface $urlBuilder
     */
    /**
     * Retrieve checkout URL
     *
     * @return string
     */
    private function getCheckoutUrl()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return $this->urlBuilder->getUrl('quotation/quote/index');
		}
	}
    /**
     * Retrieve checkout URL
     *
     * @return string
     */
    private function pageNotFoundUrl()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return $this->urlBuilder->getUrl('quotation/quote/index');
		}
	}
    /**
     * Retrieve default success page URL
     *
     * @return string
     */
    private function getDefaultSuccessPageUrl()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return $this->urlBuilder->getUrl('quotation/quote/success/');
		}
	}
}
