<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\IsProductSalableForRequestedQtyCondition;
use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Math\Division as MathDivision;
use Magento\Framework\Phrase;
use Magento\InventoryConfigurationApi\Api\Data\StockItemConfigurationInterface;
use Magento\InventoryConfigurationApi\Api\GetStockItemConfigurationInterface;
use Magento\InventoryReservationsApi\Model\GetReservationsQuantityInterface;
use Magento\InventorySalesApi\Api\Data\ProductSalabilityErrorInterfaceFactory;
use Magento\InventorySalesApi\Api\Data\ProductSalableResultInterface;
use Magento\InventorySalesApi\Api\Data\ProductSalableResultInterfaceFactory;
use Magento\InventorySalesApi\Model\GetStockItemDataInterface;
use Magento\InventorySales\Model\IsProductSalableForRequestedQtyCondition\IsCorrectQtyCondition as CorrectQtyCondition;
trait IsCorrectQtyCondition
{
    /**
     * @var GetStockItemConfigurationInterface
     */
    /**
     * @var GetReservationsQuantityInterface
     */
    /**
     * @var GetStockItemDataInterface
     */
    /**
     * @var StockConfigurationInterface
     */
    /**
     * @var MathDivision
     */
    /**
     * @var ProductSalabilityErrorInterfaceFactory
     */
    /**
     * @var ProductSalableResultInterfaceFactory
     */
    /**
     * @var RequestInterface
     */
    /**
     * IsCorrectQtyCondition constructor.
     *
     * @param GetStockItemConfigurationInterface $getStockItemConfiguration
     * @param StockConfigurationInterface $configuration
     * @param GetReservationsQuantityInterface $getReservationsQuantity
     * @param GetStockItemDataInterface $getStockItemData
     * @param MathDivision $mathDivision
     * @param ProductSalabilityErrorInterfaceFactory $productSalabilityErrorFactory
     * @param ProductSalableResultInterfaceFactory $productSalableResultFactory
     * @param RequestInterface $request
     */
    /**
     * @inheritdoc
     */
    private function execute(string $sku, int $stockId, float $requestedQty): ProductSalableResultInterface
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			/** @var StockItemConfigurationInterface $stockItemConfiguration */
        $stockItemConfiguration = $this->getStockItemConfiguration->execute($sku, $stockId);
        if ($this->isMinSaleQuantityCheckFailed($stockItemConfiguration, $requestedQty)) {
            return $this->createErrorResult(
                'is_correct_qty-min_sale_qty',
                __(
                    'The fewest you may purchase is %1',
                    $stockItemConfiguration->getMinSaleQty()
                )
            );
        }
        if ($this->isMaxSaleQuantityCheckFailed($stockItemConfiguration, $requestedQty)) {
            return $this->createErrorResult(
                'is_correct_qty-max_sale_qty',
                __('The requested qty exceeds the maximum qty allowed in shopping cart')
            );
        }
        if (!($this->request->getModuleName() == 'quotation') && !($this->request->getActionName() == 'accept')) {
            if ($this->isQuantityIncrementCheckFailed($stockItemConfiguration, $requestedQty)) {
                return $this->createErrorResult(
                    'is_correct_qty-qty_increment',
                    __(
                        'You can buy this product only in quantities of %1 at a time.',
                        $stockItemConfiguration->getQtyIncrements()
                    )
                );
            }
        }
        if ($this->isDecimalQtyCheckFailed($stockItemConfiguration, $requestedQty)) {
            return $this->createErrorResult(
                'is_correct_qty-is_qty_decimal',
                __('You cannot use decimal quantity for this product.')
            );
        }
        return $this->productSalableResultFactory->create(['errors' => []]);
		}
	}
    /**
     * Create Error Result Object
     *
     * @param string $code
     * @param Phrase $message
     * @return ProductSalableResultInterface
     */
    private function createErrorResult(string $code, Phrase $message): ProductSalableResultInterface
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$errors = [
            $this->productSalabilityErrorFactory->create([
                'code' => $code,
                'message' => $message
            ])
        ];
        return $this->productSalableResultFactory->create(['errors' => $errors]);
		}
	}
    /**
     * Check if decimal quantity is valid
     *
     * @param StockItemConfigurationInterface $stockItemConfiguration
     * @param float $requestedQty
     * @return bool
     */
    private function isDecimalQtyCheckFailed(
        StockItemConfigurationInterface $stockItemConfiguration,
        float $requestedQty
    ): bool {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return (!$stockItemConfiguration->isQtyDecimal() && (floor($requestedQty) !== $requestedQty));
		}
	}
    /**
     * Check if min sale condition is satisfied
     *
     * @param StockItemConfigurationInterface $stockItemConfiguration
     * @param float $requestedQty
     * @return bool
     */
    private function isMinSaleQuantityCheckFailed(
        StockItemConfigurationInterface $stockItemConfiguration,
        float $requestedQty
    ): bool {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			// Minimum Qty Allowed in Shopping Cart
        if ($stockItemConfiguration->getMinSaleQty() && $requestedQty < $stockItemConfiguration->getMinSaleQty()) {
            return true;
        }
        return false;
		}
	}
    /**
     * Check if max sale condition is satisfied
     *
     * @param StockItemConfigurationInterface $stockItemConfiguration
     * @param float $requestedQty
     * @return bool
     */
    private function isMaxSaleQuantityCheckFailed(
        StockItemConfigurationInterface $stockItemConfiguration,
        float $requestedQty
    ): bool {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			// Maximum Qty Allowed in Shopping Cart
        if ($stockItemConfiguration->getMaxSaleQty() && $requestedQty > $stockItemConfiguration->getMaxSaleQty()) {
            return true;
        }
        return false;
		}
	}
    /**
     * Check if increment quantity condition is satisfied
     *
     * @param StockItemConfigurationInterface $stockItemConfiguration
     * @param float $requestedQty
     * @return bool
     */
    private function isQuantityIncrementCheckFailed(
        StockItemConfigurationInterface $stockItemConfiguration,
        float $requestedQty
    ): bool {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			// Qty Increments
        $qtyIncrements = $stockItemConfiguration->getQtyIncrements();
        if ($qtyIncrements !== (float)0 && $this->mathDivision->getExactDivision($requestedQty, $qtyIncrements) !== 0) {
            return true;
        }
        return false;
		}
	}
}
