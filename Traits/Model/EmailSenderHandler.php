<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model;
/**
 * Quotation emails sending observer.
 * Performs handling of cron jobs related to sending emails to customers
 * after creation/modification of Order, Invoice, Shipment or Creditmemo.
 */
trait EmailSenderHandler
{
    /**
     * Email sender model.
     *
     * @var \Cart2Quote\Quotation\Model\Quote\Email\Sender\Sender
     */
    /**
     * Entity resource model.
     *
     * @var \Magento\Sales\Model\ResourceModel\EntityAbstract
     */
    /**
     * Entity collection model.
     *
     * @var \Magento\Sales\Model\ResourceModel\Collection\AbstractCollection
     */
    /**
     * Global configuration storage.
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    /**
     * @var \Magento\Framework\Registry
     */
    /**
     * EmailSenderHandler constructor
     *
     * @param \Cart2Quote\Quotation\Model\Quote\Email\Sender\Sender $emailSender
     * @param \Cart2Quote\Quotation\Model\ResourceModel\Quote $entityResource
     * @param \Cart2Quote\Quotation\Model\ResourceModel\Quote\Collection $entityCollection
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $globalConfig
     * @param \Magento\Framework\Registry $registry
     */
    /**
     * Handles asynchronous email sending
     *
     * @param array $ignoreStatus list of statuses that shouln't be handled by this module
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function sendEmails($ignoreStatus = [])
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			if ($this->globalConfig->getValue('sales_email/general/async_sending')) {
            $this->entityCollection->addFieldToFilter($this->emailSender->getSendEmailIdentifier(), ['eq' => 1]);
            $this->entityCollection->addFieldToFilter($this->emailSender->getEmailSentIdentifier(), ['null' => true]);
            //add statuses to ignore filter
            if (!empty($ignoreStatus)) {
                $this->entityCollection->addFieldToFilter('status', ['nin' => $ignoreStatus]);
            }
            /** @var \Magento\Sales\Model\AbstractModel $item */
            foreach ($this->entityCollection->getItems() as $item) {
                $this->registerAsyncQuotation($item);
                $this->emailSender->send($item, true);
            }
        }
		}
	}
    /**
     * Getter for the email sender
     *
     * @return \Cart2Quote\Quotation\Model\Quote\Email\Sender\Sender
     */
    private function getEmailSender()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return $this->emailSender;
		}
	}
    /**
     * Register quotation for so data can be collected in template
     *
     * @param \Magento\Framework\DataObject|\Magento\Sales\Model\AbstractModel $quote
     * @return void
     */
    private function registerAsyncQuotation($quote)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$this->coreRegistry->unregister('async_quotation');
        $this->coreRegistry->register('async_quotation', $quote);
		}
	}
}
