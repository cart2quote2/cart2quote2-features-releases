<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\Config\Source\Quote\Request\Strategy\Option;
trait QuoteList
{
    /**
     * Get label
     *
     * @return \Magento\Framework\Phrase|string
     */
    private function getLabel()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return __('No');
		}
	}
    /**
     * Get comment
     *
     * @return \Magento\Framework\Phrase|string
     */
    private function getComment()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return sprintf(
            '<b>%s:</b> %s',
            $this->getLabel(),
            __('Default quotation list option')
        );
		}
	}
}
