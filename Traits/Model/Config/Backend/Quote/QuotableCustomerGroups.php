<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\Config\Backend\Quote;
/**
 * Backend model for products quotable by default setting
 * Trait QuotableCustomerGroups
 */
trait QuotableCustomerGroups
{
    /**
     * path for quotable customer group config
     */
    /**
     * @var \Magento\Customer\Model\ResourceModel\Group\Collection
     */
    /**
     * QuotableCustomerGroups constructor.
     *
     * @param \Magento\Customer\Model\ResourceModel\Group\Collection $customerGroupCollection
     */
    /**
     * To options array
     *
     * @return array
     */
    private function toOptionArray()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return $this->customerGroupCollection->toOptionArray();
		}
	}
}
