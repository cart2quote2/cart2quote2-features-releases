<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\Config\Backend;
trait Enabled
{
    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    /**
     * Enabled constructor.
     *
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    /**
     * Save config writer before save
     *
     * @return \Magento\Framework\App\Config\Value
     */
    private function beforeSave()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$outputPath = 'advanced/modules_disable_output/Cart2Quote_Quotation';
        $this->configWriter->save($outputPath, !boolval($this->getValue()));
        return parent::beforeSave();
		}
	}
}
