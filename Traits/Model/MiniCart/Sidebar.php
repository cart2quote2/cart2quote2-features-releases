<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\MiniCart;
trait Sidebar
{
    /**
     * @var \Cart2Quote\Quotation\Model\QuotationCart
     */
    /**
     * @var \Magento\Framework\Locale\ResolverInterface
     */
    /**
     * Sidebar constructor.
     *
     * @param \Cart2Quote\Quotation\Model\QuotationCart $cart
     * @param \Magento\Framework\Locale\ResolverInterface $resolver
     */
    /**
     * Remove item from miniquote
     *
     * @param int $itemId
     */
    private function removeQuoteItem($itemId)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$this->cart->removeItem($itemId)->save();
        $this->cart->save();
		}
	}
    /**
     * Update miniquote item quantity
     *
     * @param int $itemId
     * @param int $itemQty
     * @return \Magento\Quote\Model\Quote\Item|string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function updateQuoteItem($itemId, $itemQty)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			if (isset($itemQty) && !is_float($itemQty)) {
            $filter = new \Magento\Framework\Filter\LocalizedToNormalized(
                [
                    'locale' => $this->resolver->getLocale()
                ]
            );
            $itemQty = $filter->filter($itemQty);
        }
        $item = $this->cart->updateItems([$itemId => ['qty' => $itemQty]]);
        if (is_string($item)) {
            throw new \Magento\Framework\Exception\LocalizedException(__($item));
        }
        if ($item->getHasError()) {
            throw new \Magento\Framework\Exception\LocalizedException(__($item->getMessage()));
        }
        $this->cart->save();
        return $item;
		}
	}
    /**
     * Compile response data
     *
     * @param string $error
     * @return array
     */
    private function getResponseData($error = '')
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			if (empty($error)) {
            $response = [
                'success' => true,
            ];
        } else {
            $response = [
                'success' => false,
                'error_message' => $error,
            ];
        }
        return $response;
		}
	}
}
