<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model;
use Cart2Quote\Quotation\Api\QuoteItemUpdaterInterface;
use Magento\Quote\Model\Quote\ItemFactory;
use Cart2Quote\Quotation\Model\Quote\Item\Updater;
trait QuoteItemUpdater
{
    /**
     * @var \Magento\Quote\Model\Quote\ItemFactory
     */
    /**
     * @var \Cart2Quote\Quotation\Model\Quote\Item\Updater
     */
    /**
     * @param ItemFactory $itemFactory
     * @param Updater $quoteItemUpdater
     */
    /**
     * Update quote item by quote ID and item ID.
     *
     * @param int $quoteId
     * @param int $itemId
     * @param array $itemData
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function updateQuoteItem($quoteId, $itemId, array $itemData)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$quoteItem = $this->itemFactory->create()->load($itemId);
        if (!$quoteItem->getId() || $quoteItem->getQuoteId() != $quoteId) {
            throw new \Magento\Framework\Exception\NoSuchEntityException(__('Quote item does not exist.'));
        }
        $this->quoteItemUpdater->update($quoteItem, $itemData);
        return true;
		}
	}
}
