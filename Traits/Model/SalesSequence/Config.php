<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\SalesSequence;
/**
 * Trait Config
 * - configuration container for sequence
 *
 */
trait Config
{
    /**
     * Default sequence values
     * - Prefix represents prefix for sequence: AA000
     * - Suffix represents suffix: 000AA
     * - startValue represents initial value
     * - warning value will be using for alert messages when increment closing to overflow
     * - maxValue represents last available increment id in system
     *
     * @var array
     */
    /**
     * Default toOptionArray function
     *
     * @return array
     */
    private function toOptionArray()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			return [];
		}
	}
}
