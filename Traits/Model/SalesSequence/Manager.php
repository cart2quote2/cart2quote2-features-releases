<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\SalesSequence;
trait Manager
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    /**
     * @var \Magento\SalesSequence\Model\ResourceModel\Meta
     */
    /**
     * @var \Magento\SalesSequence\Model\SequenceFactory
     */
    /**
     * @var \Magento\Eav\Model\Entity\Type
     */
    /**
     * @var \Cart2Quote\Quotation\Helper\Data
     */
    /**
     * Manager constructor
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\SalesSequence\Model\ResourceModel\Meta $resourceSequenceMeta
     * @param \Magento\SalesSequence\Model\SequenceFactory $sequenceFactory
     * @param \Magento\Eav\Model\Entity\Type $entityConfig
     * @param \Cart2Quote\Quotation\Helper\Data $helperData
     */
    /**
     * Returns sequence for given entityType and store
     *
     * @param string $entityType
     * @param int $storeId
     * @return \Magento\Framework\DB\Sequence\SequenceInterface|\Magento\SalesSequence\Model\Sequence
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getSequence($entityType, $storeId)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$sequence = $this->sequenceFactory->create(
            [
                'meta' => $this->resourceSequenceMeta->loadByEntityTypeAndStore(
                    $entityType,
                    $storeId
                ),
                'entityType' => $this->entityConfig->loadByCode($entityType)
            ]
        );
        $prefix = $this->helperData->getQuotePrefix($storeId);
        if (isset($prefix)) {
            $sequence->setPrefix($prefix);
        }
        return $sequence;
		}
	}
}
