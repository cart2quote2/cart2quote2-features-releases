<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\Email;
trait ZendOne
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    /**
     * @var \Magento\Framework\Filesystem\DriverInterface
     */
    /**
     * @var bool
     */
    /**
     * ZendOne constructor
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    /**
     * Function to attach a file to an outgoing email
     *
     * @param string $file
     * @param string $name
     * @return \Laminas\Mime\Part|void|\Zend_Mime_Part
     */
    private function attachFileAdapter($file, $name)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			if (!empty($file) && $this->fileSystem->isFile($file)) {
            $fileContents = $this->fileSystem->fileOpen($file, 'r');
            if ($this->isLaminas) {
                $attachment = new \Laminas\Mime\Part($fileContents);
                $attachment->type = \Laminas\Mime\Mime::TYPE_OCTETSTREAM;
                $attachment->encoding = \Laminas\Mime\Mime::ENCODING_BASE64;
                $attachment->disposition = \Laminas\Mime\Mime::DISPOSITION_ATTACHMENT;
                $attachment->filename = $name;
            }else{
                $attachment = new \Zend_Mime_Part($fileContents);
                $attachment->type = \Zend_Mime::TYPE_OCTETSTREAM;
                $attachment->encoding = \Zend_Mime::ENCODING_BASE64;
                $attachment->disposition = \Zend_Mime::DISPOSITION_ATTACHMENT;
                $attachment->filename = $name;
            }
            return $attachment;
        }
		}
	}
    /**
     * Get message adapter
     *
     * @param array $attachedPart
     * @param string $body
     * @param null|\Magento\Framework\Mail\Message $message
     */
    private function getMessageAdapter($attachedPart, $body, $message = null)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			if ($message === null) {
            return;
        }
        if ($this->isLaminas) {
            $message->setMessageType(\Laminas\Mime\Mime::TYPE_HTML);
            $message->setBody($body);
            if (!empty($attachedPart)) {
                foreach ($attachedPart as $part) {
                    $message->addAttachment($part);
                }
            }
        } else {
            $message->setMessageType(\Zend_Mime::TYPE_HTML);
            $message->setBody($body);
            if (!empty($attachedPart)) {
                foreach ($attachedPart as $part) {
                    $message->addAttachment($part);
                }
            }
        }
		}
	}
}
