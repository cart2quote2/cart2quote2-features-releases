<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\Email;
/**
 * Trait ZendAdapter
 *
 * @package Cart2Quote\Quotation\Model\Email
 */
trait ZendAdapter
{
    /**
     * Attach file adapter
     *
     * @param string $file
     * @param string $name
     * @return mixed
     */
    abstract public function attachFileAdapter($file, $name);
    /**
     * Get message adapter
     *
     * @param array $attachedPart
     * @param string $body
     * @param \Magento\Framework\Mail\Message|null $message
     * @return mixed
     */
    abstract public function getMessageAdapter($attachedPart, $body, $message = null);
}
