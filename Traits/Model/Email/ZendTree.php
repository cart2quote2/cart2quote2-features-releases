<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\Email;
use Magento\Framework\Mail\MimePartInterfaceFactory;
trait ZendTree
{
    /**
     * @var \Magento\Framework\Mail\MimePartInterfaceFactory
     */
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    /**
     * @var \Magento\Framework\Filesystem\DriverInterface
     */
    /**
     * @var bool
     */
    /**
     * ZendTree constructor.
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    /**
     * Get attach file adapter
     *
     * @param string $file
     * @param string $name
     * @return \Zend\Mime\Part
     */
    private function attachFileAdapter($file, $name)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			if (!empty($file) && $this->fileSystem->isFile($file)) {
            $fileContents = $this->fileSystem->fileOpen($file, 'r');
            if ($this->isLaminas) {
                $attachment = $this->mimePartInterfaceFactory->create(
                    [
                        'content' => $fileContents,
                        'type' => \Laminas\Mime\Mime::TYPE_OCTETSTREAM,
                        'fileName' => $name,
                        'disposition' => \Laminas\Mime\Mime::DISPOSITION_ATTACHMENT,
                        'encoding' => \Laminas\Mime\Mime::ENCODING_BASE64
                    ]
                );
            }else{
                $attachment = $this->mimePartInterfaceFactory->create(
                    [
                        'content' => $fileContents,
                        'type' => \Zend\Mime\Mime::TYPE_OCTETSTREAM,
                        'fileName' => $name,
                        'disposition' => \Zend\Mime\Mime::DISPOSITION_ATTACHMENT,
                        'encoding' => \Zend\Mime\Mime::ENCODING_BASE64
                    ]
                );
            }
            return $attachment;
        }
		}
	}
    /**
     * Get message adapter
     *
     * @param array $attachedPart
     * @param string $body
     * @param \Magento\Framework\Mail\Message|\Zend\Mime\Message|null $message
     */
    // @codingStandardsIgnoreStart
    private function getMessageAdapter($attachedPart, $body, $message = null)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			// This method intentionally left empty as it's not needed for our use case, Adapter not needed after Magento 2.3.3 and higher
		}
	}
    // @codingStandardsIgnoreEnd
}
