<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\Email;
trait ZendTwo
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    /**
     * @var \Magento\Framework\Filesystem\DriverInterface
     */
    /**
     * @var bool
     */
    /**
     * ZendTwo constructor
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    /**
     * Get attach file adapter
     *
     * @param string $file
     * @param string $name
     * @return \Zend\Mime\Part|\Laminas\Mime\Part|null
     */
    private function attachFileAdapter($file, $name)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			if (!empty($file) && $this->fileSystem->isFile($file)) {
            $fileContents = $this->fileSystem->fileGetContents($file);
            if ($this->isLaminas) {
                $attachment = new \Laminas\Mime\Part($fileContents);
                $attachment->type = \Laminas\Mime\Mime::TYPE_OCTETSTREAM;
                $attachment->encoding = \Laminas\Mime\Mime::ENCODING_BASE64;
                $attachment->disposition = \Laminas\Mime\Mime::DISPOSITION_ATTACHMENT;
            } else {
                $attachment = new \Zend\Mime\Part($fileContents);
                $attachment->type = \Zend\Mime\Mime::TYPE_OCTETSTREAM;
                $attachment->encoding = \Zend\Mime\Mime::ENCODING_BASE64;
                $attachment->disposition = \Zend\Mime\Mime::DISPOSITION_ATTACHMENT;
            }
            $attachment->filename = $name;
            return $attachment;
        }
        return null;
		}
	}
    /**
     * Get message adapter
     *
     * @param array $attachedPart
     * @param string $body
     * @param \Magento\Framework\Mail\Message|\Zend\Mime\Message|\Laminas\Mime\Message|null $message
     */
    private function getMessageAdapter($attachedPart, $body, $message = null)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			if ($this->isLaminas) {
            $mimeMessage = new \Laminas\Mime\Message();
            $mimePart = new \Laminas\Mime\Part($body);
            $mimePart->type = \Laminas\Mime\Mime::TYPE_HTML;
            $mimePart->charset = 'utf-8';
        } else {
            $mimeMessage = new \Zend\Mime\Message();
            $mimePart = new \Zend\Mime\Part($body);
            $mimePart->type = \Zend\Mime\Mime::TYPE_HTML;
            $mimePart->charset = 'utf-8';
        }
        $mimeMessage->setParts([$mimePart]);
        if (!empty($attachedPart)) {
            foreach ($attachedPart as $part) {
                $mimeMessage->addPart($part);
            }
        }
        $message->setBody($mimeMessage);
		}
	}
}
