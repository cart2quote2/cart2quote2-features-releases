<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\ResourceModel\Quote\Customer\Grid;
trait Collection
{
    /**
     * @var \Magento\Framework\Registry
     */
    /**
     * Collection constructor.
     *
     * @param \Magento\Framework\DB\Helper $coreResourceHelper
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param string $mainTable
     * @param string $resourceModel
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    /**
     * Initialize db select
     *
     * @return $this
     */
    private function _initSelect()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			parent::_initSelect();
        $this->addCustomerIdFilter(
            $this->registry->registry(
                \Cart2Quote\Quotation\Controller\Adminhtml\Quote\Customer\Grid::CURRENT_CUSTOMER_ID
            )
        );
        return $this;
		}
	}
    /**
     * Add filtration by customer id
     *
     * @param int $customerId
     * @return $this
     */
    private function addCustomerIdFilter($customerId)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$this->getSelect()->where(
            'q.customer_id = ?',
            $customerId
        );
        return $this;
		}
	}
}
