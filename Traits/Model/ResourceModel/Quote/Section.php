<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\ResourceModel\Quote;
/**
 * Section resourcemodel
 */
trait Section
{
    /**
     * @var string
     */
    /**
     * @var string
     */
    /**
     * Internal constructor
     *
     * @return void
     */
    private function _construct()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$this->_init('quotation_quote_sections', 'section_id');
        $this->itemsTable = $this->getTable('quotation_quote_sections', $this->connectionName);
		}
	}
    /**
     * Section constructor.
     *
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param null|string $connectionName
     */
    // TODO: Fix UselessOverridingMethod
    // phpcs:disable Generic.CodeAnalysis.UselessOverridingMethod
    /**
     * Unassigned exits for quote
     *
     * @param string|int $quoteId
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Db_Statement_Exception
     */
    private function unassignedExistsForQuote($quoteId)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$select = $this->getConnection()->select()
            ->from($this->getMainTable())
            ->where(\Cart2Quote\Quotation\Api\Data\Quote\SectionInterface::QUOTE_ID . ' = ?', $quoteId)
            ->where(\Cart2Quote\Quotation\Api\Data\Quote\SectionInterface::IS_UNASSIGNED . ' = ?', true);
        return $select->query()->rowCount() > 0;
		}
	}
}
