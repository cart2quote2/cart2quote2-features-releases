<?php
/*
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\ResourceModel\Cron;
/**
 * Collection resource model
 *
 */
trait Collection
{
    /**
     * @var string
     */
    /**
     * Collection model initialization
     *
     * @return void
     */
    private function _construct()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$this->_init(
            \Magento\Cron\Model\Schedule::class,
            \Magento\Cron\Model\ResourceModel\Schedule::class
        );
		}
	}
}
