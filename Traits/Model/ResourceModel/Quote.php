<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\ResourceModel;
/**
 * Quote resource model
 *
 */
trait Quote
{
    /**
     * Prefix for resources that will be used in this resource model
     *
     * @var string
     */
    /**
     * Use is object new method for save of object
     *
     * @var bool
     */
    /**
     * Primary key auto increment flag
     *
     * @var bool
     */
    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    /**
     * @var \Magento\Quote\Model\ResourceModel\Quote
     */
    /**
     * Quote constructor
     *
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     * @param \Magento\Quote\Model\ResourceModel\Quote $quoteResourceModel
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Magento\Framework\Model\ResourceModel\Db\VersionControl\Snapshot $entitySnapshot
     * @param \Magento\Framework\Model\ResourceModel\Db\VersionControl\RelationComposite $entityRelationComposite
     * @param \Magento\SalesSequence\Model\Manager $sequenceManager
     * @param string $connectionName
     */
    /**
     * Save
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    private function save(\Magento\Framework\Model\AbstractModel $object)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			//quote_id refers to the quotation_quote whereas the entity_id is the magento quote
        if ($object instanceof \Cart2Quote\Quotation\Model\Quote) {
            if (!$object->getQuoteId()) {
                $quote = $this->quoteFactory->create();
                $quote->setData($object->getData());
                $this->quoteResourceModel->save($quote);
                $object->setQuoteId($quote->getId());
            }
            return parent::save($object);
        }
        $this->quoteResourceModel->save($object);
        return $this;
		}
	}
    /**
     * Initialize table and PK name
     *
     * @return void
     */
    private function _construct()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$this->_init('quotation_quote', 'quote_id');
		}
	}
    /**
     * Retrieve select object for load object data
     *
     * @param string $field
     * @param mixed $value
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return \Magento\Framework\DB\Select
     */
    private function _getLoadSelect($field, $value, $object)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$mainTable = $this->getMainTable();
        $field = $this->getConnection()->quoteIdentifier(sprintf('%s.%s', $mainTable, $field));
        $select = $this->getConnection()->select()
            ->from($mainTable)
            ->joinLeft(
                ['q' => $this->quoteResourceModel->getTable('quote')],
                '`q`.entity_id = ' . $mainTable . '.quote_id'
            )
            ->where($field . '=?', $value);
        return $select;
		}
	}
    /**
     * Perform actions before object save
     *
     * @param \Magento\Framework\Model\AbstractModel|\Magento\Framework\Object $object
     * @return $this
     */
    private function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			if ($object instanceof \Cart2Quote\Quotation\Model\EntityInterface && $object->getIncrementId() == null) {
            $newIncrementId = $this->sequenceManager->getSequence(
                $object->getEntityType(),
                $object->getStoreId()
            )->getNextValue();
            $object->setIncrementId($newIncrementId);
        }
        parent::_beforeSave($object);
        return $this;
		}
	}
}
