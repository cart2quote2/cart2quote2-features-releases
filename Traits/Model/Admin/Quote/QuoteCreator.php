<?php
/**
 * Copyright (c) 2025. Cart2Quote B.V. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Cart2Quote\Features\Traits\Model\Admin\Quote;
trait QuoteCreator
{
    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    /**
     * QuoteCreator constructor.
     *
     * @param \Magento\Backend\Model\Auth\Session $authSession
     */
    /**
     * Get the creator of the quote
     *
     * @return \Magento\Framework\Phrase
     */
    private function getQuoteCreator()
    {
		if(\Cart2Quote\License\Model\License::getInstance()->isValid()) {
			$user = $this->authSession->getUser();
        $userFullName = $user ? $user->getName() : 'Admin';
        $userRole = $user ? $user->getRole()->getRoleName() : 'role';
        return __('%1: %2', $userRole, $userFullName);
		}
	}
}
